﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

	public Transform player;
    public Rigidbody2D bossRb;
	public bool isFlipped = false;
    private float agroRange = 15f;
    public float speed = 1f;
    

    
    private void Update()
    {
        float disToPlayer = Vector2.Distance(transform.position, player.position);
        if (disToPlayer < agroRange)
        {
            //chase the player
            ChasePlayer();
        }
        else
        {
            StopChasingPlayer();
            //stop chasing player
        }
        LookAtPlayer();
        
    }
    public void LookAtPlayer()
	{
		Vector3 flipped = transform.localScale;
		flipped.z *= -1f;

		if (transform.position.x > player.position.x && isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = false;
		}
		else if (transform.position.x < player.position.x && !isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = true;
		}
        
    }

    private void StopChasingPlayer()
    {
        bossRb.velocity = new Vector2(0, bossRb.velocity.y);
    }

    private void ChasePlayer()
    {
        if (transform.position.x < player.position.x)
        {
        
            bossRb.velocity = new Vector2(speed, bossRb.velocity.y);
        }
        else
        {
            
            bossRb.velocity = new Vector2(-speed, bossRb.velocity.y);
        }
    }
   
}
