﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public GameObject boss;
    private void Start()
    {
        
        boss.gameObject.SetActive(false);
    }
    private void Update()
    {
        if (PlayerController.Instance.playerHitCheckPoint2)
        {
            boss.gameObject.SetActive(true);
        }
    }
}
