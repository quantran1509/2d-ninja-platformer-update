using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BossHealth : MonoBehaviour
{
    [SerializeField] Image UIBossHealth;

    public float bossHealth = 500;
    public float currentBossHP;
    public GameObject deathEffect;
    public GameObject hurtEffect;
    public Text youWin;
    public bool angry;
    
    private void Start()
    {
        currentBossHP = bossHealth;
        youWin.gameObject.SetActive(false);
 
    }
    private void Update()
    {
        UIBossHealth.fillAmount = (currentBossHP/ bossHealth);
        Physics2D.IgnoreLayerCollision(10, 11);
        if (currentBossHP <= 200)
        {
            angry = true;
            GetComponent<Animator>().SetBool("IsEnraged", true);
            
        }

        if (currentBossHP <= 0)
        {
            Die();
           
        }
       
        
    }
    
	void Die()
	{
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
        youWin.gameObject.SetActive(true);
        youWin.text = "YOU WON!";
        Time.timeScale = 0;
    }
   
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Weapons"))
        {
            currentBossHP -= 50f;
            Destroy(collision.gameObject);
            Instantiate(hurtEffect, transform.position, Quaternion.identity);
        }
        
    }
   
}
