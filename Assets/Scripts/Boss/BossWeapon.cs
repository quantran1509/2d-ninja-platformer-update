﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWeapon : MonoBehaviour
{

	public Vector3 attackOffset;
	public float attackRange = 0.5f;
	public LayerMask playerMask;
    public Animator bossAnim;
    public float bossAttackRate = 1.5f;
    private float currentTimer;
    BossHealth bosshealth;
    Boss boss;
    public AudioSource introBossSound;
    
    private void Start()
    {
        Invoke("IntroBossSound", 1f);
        currentTimer = bossAttackRate;
        bosshealth = GetComponent<BossHealth>();
        boss = GetComponent<Boss>();
    }
    private void Update()
    {
        currentTimer += Time.deltaTime;
        if (currentTimer > bossAttackRate)
        {

            Attack();
            if (bosshealth.angry)
            {
                EnragedAttack();
            } 
        }
        
       
    }
    public void Attack()
	{
		Vector3 pos = transform.position;
		pos += transform.right * attackOffset.x;
		pos += transform.up * attackOffset.y;

		Collider2D hit = Physics2D.OverlapCircle(pos, attackRange, playerMask);
		if (hit != null)
		{
            bossAnim.SetTrigger("Attack");
            UIManager.FindObjectOfType<UIManager>().playerHealth -= 5;
            PlayerController.Instance.GetComponent<Animator>().SetTrigger("Hurt");
            PlayerController.Instance.playerHurtSound.Play();
            currentTimer = 0;
        }
	}

	public void EnragedAttack()
	{
        
        Vector3 pos = transform.position;
		pos += transform.right * attackOffset.x;
		pos += transform.up * attackOffset.y;
        boss.speed = 2f;
		Collider2D hit = Physics2D.OverlapCircle(pos, attackRange, playerMask);
		if (hit != null)
		{
            bossAnim.SetTrigger("EnragedAttack");
            UIManager.FindObjectOfType<UIManager>().playerHealth -= 10;
            PlayerController.Instance.GetComponent<Animator>().SetTrigger("Hurt");
            PlayerController.Instance.playerHurtSound.Play();
            currentTimer = 0;
        }
	}
    void IntroBossSound()
    {
        introBossSound.Play();
    }
    void OnDrawGizmosSelected()
	{
		Vector3 pos = transform.position;
		pos += transform.right * attackOffset.x;
		pos += transform.up * attackOffset.y;

		Gizmos.DrawWireSphere(pos, attackRange);
	}
}
