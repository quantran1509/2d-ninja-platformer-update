﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenSpawner : MonoBehaviour
{
    public GameObject shurikenPrefab;
    public Transform player;
    private float min_Y = -10f;
    private float max_Y = -2f;
    public float shurikenSpawnTimer = 1.5f;
    private float currentTime;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = shurikenSpawnTimer;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        float distancetoPlayer = Vector2.Distance(transform.position, player.position);
        /* Debug.LogError(distancetoPlayer);*/
        if (distancetoPlayer < 22)
        {
            Vector2 temp = transform.position;
            temp.x += 10 * Time.deltaTime;
            transform.position = temp;
            if (currentTime > shurikenSpawnTimer)
            {
                SpawnShuriken();
                currentTime = 0;
            }
        }
        else if (distancetoPlayer > 22)
        {
            Vector2 temp = transform.position;
            temp.x -= 2 * Time.deltaTime;
            transform.position = temp;
            if (currentTime > shurikenSpawnTimer)
            {
                SpawnShuriken();
                currentTime = 0;
            }
        }

    }
    void SpawnShuriken()
    {
        float pos_Y = Random.Range(min_Y, max_Y);
        Vector2 temp = transform.position;
        temp.y = pos_Y;
        Instantiate(shurikenPrefab, temp, Quaternion.identity);
    }
}
