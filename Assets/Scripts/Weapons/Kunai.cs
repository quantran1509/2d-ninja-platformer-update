﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunai : MonoBehaviour
{
    public float speed;
    public bool rightKunai, leftKunai;

    private void Start()
    {
        Invoke("Destroy", 3f);
    }
    void Update()
    {
        if (rightKunai)
        {
            Vector2 temp = transform.position;
            temp.x += speed * Time.deltaTime;
            transform.position = temp;
        }
        if (leftKunai)
        {
            Vector2 temp = transform.position;
            temp.x -= speed * Time.deltaTime;
            transform.position = temp;
        }
    }
    private void Destroy()
    {
        gameObject.SetActive(false);
    }
}
