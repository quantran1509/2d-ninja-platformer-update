﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{

    [SerializeField] Image UIHealth;
    public float playerHealth;
    public float playerMaxHealth = 50f;

    private void Awake()
    {
        
    }
    void Start()
    {
        playerHealth = playerMaxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        UIHealth.fillAmount = (playerHealth / playerMaxHealth);
        if (playerHealth <= 0)
        {
            StartCoroutine(Death());
        }
    }

    
    IEnumerator Death()
    {
        PlayerController.Instance.GetComponent<Animator>().SetTrigger("Dead");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
