﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transitionAnim;
    [SerializeField] int transitionTime = 2;
    // Update is called once per frame
    private void Awake()
    {
     
    }
    void Update()
    {
        if (PlayerController.Instance.playerHitCheckPoint1)
        {
            LoadNextLevel();
        }     
    }
    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }
    IEnumerator LoadLevel(int levelIndex)
    {
        // Play Animation
        transitionAnim.SetTrigger("Start");
        // Wait
        yield return new WaitForSeconds(transitionTime);
        // Load Scene
        SceneManager.LoadScene(levelIndex);
    }
    
}
