﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseButton : MonoBehaviour
{
    public Text pauseText;
    private void Start()
    {
        pauseText.gameObject.SetActive(false);
    }
    public void OnPauseButtonClick()
    {
        if (Time.timeScale == 1)
        {

            Time.timeScale = 0;
            pauseText.gameObject.SetActive(true);
            pauseText.text = "Paused";

        }
        else if (Time.timeScale == 0)
        {
            pauseText.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }
    public void OnRestartButtonClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    public void OnLoadMainMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
   
}
