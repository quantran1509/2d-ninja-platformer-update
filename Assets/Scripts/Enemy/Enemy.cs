﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] Transform groundCheckPoint;
    [SerializeField] Transform findPlayerPoint;
    [SerializeField] Transform leftKunai, rightKunai;
    [SerializeField] Transform attackPoint;
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Animator enemyAnim;
    [SerializeField] Rigidbody2D enemyRb;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] float speed;
    [SerializeField] float dir;
    [SerializeField] float attackRate = 2f;
    [SerializeField] int enemyHealth;
    [SerializeField] Transform healPrefab;
    public AudioSource enemyHurtSound;

    bool isGrounded;
    bool facingRight;
    private float currentTime;

    void Start()
    {
        facingRight = true;
        dir = 1f;
        currentTime = attackRate;
        enemyHealth = 3;
    }

    // Update is called once per frame
    void Update()
    {
        FindPlayer();
        CheckGround();
        Move(dir);
        if (!isGrounded)
        {
            Flip(dir);
            dir = -1f;
            if (facingRight)
            {
                dir = 1;
            }
           
        }
        if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack") || enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Hurt") || enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            enemyRb.velocity = new Vector2(0, enemyRb.velocity.y);
        }


    }
    void Move(float dir)
    {
        enemyRb.velocity = new Vector2(speed * dir, enemyRb.velocity.y);
        enemyAnim.SetFloat("Speed", Mathf.Abs(dir));

    }
    void EnemyAttack()
    {
        if (facingRight)
        {
            Instantiate(rightKunai, attackPoint.position, Quaternion.Euler(0, 0, -90));
        }
        if (!facingRight)
        {
            Instantiate(leftKunai, attackPoint.position, Quaternion.Euler(0, 0, 90));
        }
    }

   void CheckGround()
    {
        Vector2 direction = Vector2.down;
        float distance = 3.0f;

        RaycastHit2D hit = Physics2D.Raycast(groundCheckPoint.position, direction, distance, groundLayer);
        if (hit.collider != null)
        {
            Debug.DrawRay(groundCheckPoint.position, direction, Color.green);
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
            
            
        }
    }
    void FindPlayer()
    {
        currentTime += Time.deltaTime;
        float distance = 3.0f;
        Vector2 endPos = findPlayerPoint.position + Vector3.right * dir * distance;
        RaycastHit2D hitPlayer = Physics2D.Linecast(findPlayerPoint.position, endPos, 1 << LayerMask.NameToLayer("Player"));
        if (hitPlayer.collider != null)
        {
            Debug.DrawLine(findPlayerPoint.position, endPos, Color.blue);
            
            if (currentTime > attackRate)
            {
                // Attack
                EnemyAttack();
                // Play Attack Anim
                enemyAnim.SetTrigger("Attack");
                currentTime = 0;
            }
        }
            

    }
    void Flip(float dir)
    {
        //check face direction and flip the player
        if (dir > 0 && facingRight || dir < 0 && !facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
       
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Weapons"))
        {
            enemyHealth--;
            Destroy(collision.gameObject);
            if (enemyHealth <= 0)
            {
                KillShelf();

            }
            else
            {
                enemyHurtSound.Play();
                Hurt();
            }
        }
    }
    private void Destroy()
    {
        gameObject.SetActive(false);
        Instantiate(healPrefab, transform.position, Quaternion.identity);
    }
    void Hurt()
    {
        //Play Hurt anim
        enemyAnim.SetTrigger("Hurt");
        
    }
    private void KillShelf()
    {
        //Play anim die
        enemyAnim.SetTrigger("Die");
        Invoke("Destroy", 1f);

    }
}
