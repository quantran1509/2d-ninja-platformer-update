﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarFollowBoss : MonoBehaviour
{
    public Transform boss;
    private void Update()
    {
        Vector2 temp = transform.position;
        temp.x = boss.transform.position.x;
        temp.y = boss.transform.position.y + 5f;
        transform.position = temp;
        
    }

}
