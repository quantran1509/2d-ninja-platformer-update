﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    public PlayerController player;

   
    private void OnTriggerStay2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Platforms"))
        {
            player.isJumping = false;
            player.isGrounded = true;

        }
        else if (collision.gameObject.CompareTag("MovingPlatforms"))
        {
            player.transform.parent = collision.gameObject.transform;
            player.isJumping = false;
            player.isGrounded = true;
        }
        else
        {
            player.isGrounded = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatforms"))
        {
            player.transform.parent = null;
        }

    }

}
