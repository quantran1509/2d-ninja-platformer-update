﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour,Controls.IPlayerActions
{
    public static PlayerController Instance;
    public Controls controls;
    private Rigidbody2D playerRb;
    private Animator playerAnim;
    public bool isGrounded;
    public GameObject rightKunai, leftKunai;
    public SpriteRenderer spriteRenderer;
    public float jumpHeight;
    public float speed;
    public Transform throwPoint;
    float moveDirection;
    private bool facingRight;
    public bool isJumping;
    private bool jumpAttack;
    public float attackRate = 1f;
    private float currentTime;
    public bool playerHitCheckPoint1;
    public bool playerHitCheckPoint2;
    public AudioSource playerThrowSound;
    public AudioSource playerHurtSound;
    public AudioSource playerCollectSound;


    private void OnEnable()
    {     
        controls.Player.Enable();
    }
    private void OnDisable()
    {
        controls.Player.Disable();
    }
   
    private void Awake()
    {
        
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        controls = new Controls();
        controls.Player.SetCallbacks(this);
    }
    private void Start()
    {
        facingRight = true;
        playerRb = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();
        currentTime = attackRate;
    }
    public void OnJump(InputAction.CallbackContext context)
    {
        if (!isJumping && isGrounded)
        {
            //Jump
            playerRb.velocity = new Vector2(playerRb.velocity.x, jumpHeight * Time.fixedDeltaTime);
            //Check jumping
            isJumping = true;
            isGrounded = false;
            //Play anim jump
            playerAnim.SetTrigger("Jump");
            playerAnim.SetBool("Jump Throw", false);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        //Read the value from keyboard
        moveDirection = context.ReadValue<float>();
    }
    public void OnRangeAttack(InputAction.CallbackContext context)
    {
        jumpAttack = true;
        if (currentTime > attackRate)
        {
            if (context.started && facingRight)
            {
                //Throw a knife
                Instantiate(rightKunai, throwPoint.transform.position, Quaternion.Euler(0, 0, -90));
                //Play anim throw
                playerAnim.SetTrigger("Throw");
                playerThrowSound.Play();
                currentTime = 0;
            }
            if (context.started && !facingRight)
            {
                Instantiate(leftKunai, throwPoint.transform.position, Quaternion.Euler(0, 0, 90));
                //Play anim throw
                playerAnim.SetTrigger("Throw");
                playerThrowSound.Play();
                currentTime = 0;
            }
            if (jumpAttack && !isGrounded && !playerAnim.GetCurrentAnimatorStateInfo(1).IsName("Jump Throw"))
            {
                playerAnim.SetBool("Jump Throw", true);
                currentTime = 0;
            }
            if (!jumpAttack && !playerAnim.GetCurrentAnimatorStateInfo(1).IsName("Jump Throw"))
            {
                playerAnim.SetBool("Jump Throw", false);
                currentTime = 0;
            }
        }
        
    }
    
    public void OnSlide(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            playerAnim.SetBool("Slide", true);
        }
    }
    private void Update()
    {
        Flip(moveDirection);
        HandleLayer();
        currentTime += Time.deltaTime;
        if (playerRb.velocity.y < -0.1f)
        {
            isGrounded = false;
            playerAnim.SetBool("Land", true);
           
        }
       
        if (isGrounded)
        {
            playerAnim.ResetTrigger("Jump");
            playerAnim.SetBool("Land", false);
        }

    }
    private void FixedUpdate()
    {
        
        if (moveDirection > 0)
        {
            facingRight = true;
            playerRb.velocity = new Vector2(speed * moveDirection, playerRb.velocity.y);
            playerAnim.SetFloat("Speed", Mathf.Abs(moveDirection));
            
        }
        else if (moveDirection < 0)
        {
            facingRight = false;
            playerRb.velocity = new Vector2(speed * moveDirection, playerRb.velocity.y);
            playerAnim.SetFloat("Speed", Mathf.Abs(moveDirection));
        }
        else
        {
            playerAnim.SetFloat("Speed", Mathf.Abs(moveDirection));
            playerAnim.SetBool("Slide", false);
            playerRb.velocity = new Vector2(0, playerRb.velocity.y);
        }
        if (playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attack") || playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Throw"))
        {
            playerRb.velocity = new Vector2(0, playerRb.velocity.y);
        }
        

    }
    void Flip(float moveDirection)
    {
        //check face direction and flip the player
        if (moveDirection > 0 && !facingRight || moveDirection < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
    void HandleLayer()
    {
        if (!isGrounded)
        {
            playerAnim.SetLayerWeight(1, 1);

        }
        else if (isGrounded)
        {
            playerAnim.SetLayerWeight(1, 0);
        }

    }
    //Check if the player get hurt and -health
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Weapons"))
        {
            UIManager.FindObjectOfType<UIManager>().playerHealth -= 5;
            Destroy(collision.gameObject);

            // Play anim hurt
            playerAnim.SetTrigger("Hurt");
            playerHurtSound.Play();
        }
        if (collision.gameObject.CompareTag("Heart"))
        {
            Destroy(collision.gameObject);
            UIManager.FindObjectOfType<UIManager>().playerHealth += 10;
            playerCollectSound.Play();
        }
        if (collision.gameObject.CompareTag("Shuriken"))
        {
            Destroy(collision.gameObject);
            playerAnim.SetTrigger("Hurt");
            playerHurtSound.Play();
            UIManager.FindObjectOfType<UIManager>().playerHealth -= 5;
        }
    }
    //Chech if the player hit the CheckPoint 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("CheckPoint1"))
        {
            playerHitCheckPoint1 = true;
        }
            if (collision.gameObject.CompareTag("CheckPoint2"))
        {
            playerHitCheckPoint2 = true;
            collision.gameObject.SetActive(false);
            attackRate = 0.5f;
        }
        if (collision.gameObject.CompareTag("DeathPoint"))
        {
            
            Time.timeScale = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }
    }
    

}
